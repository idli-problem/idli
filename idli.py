import sys
p = "4 6 8 8"
pattern = list(map(int, p.split()))

pat = [i % 2 for i in pattern]
print(pat)
countOdd = pat.count(1)
if(countOdd % 2):
    print("IP")
    sys.exit()
flag = 0
counter = 0
sum = 0
for x in pat:
    if x % 2:
        if flag == 0:
            flag = 1
        else :
            sum += 2 * counter + 2
            flag = 0
            counter =0
    else:
        if flag == 1:
            counter += 1

print(sum)
